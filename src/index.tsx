import React from 'react';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Dashboard from './dashboard/Dashboard';
import {render} from "react-dom";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import ScooterDetails from './dashboard/scooter-details/ScooterDetails';
import ScootersMaps from './dashboard/scooters-maps/ScootersMaps';

const rootElement = document.getElementById("root");
render(
    <BrowserRouter>
        <Switch>
            <Route exact path="/">
                <Dashboard/>
            </Route>
            <Route exact path="/scooters-fleet-maps">
                <ScootersMaps/>
            </Route>
            <Route exact path="/:scooterId">
                <ScooterDetails/>
            </Route>
        </Switch>
    </BrowserRouter>,
    rootElement
);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
