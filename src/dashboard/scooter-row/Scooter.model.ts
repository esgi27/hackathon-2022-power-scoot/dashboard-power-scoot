export class Scooter {
    id: number;
    scooterId: string;
    scooterHistoric: ScooterHistory[];

    constructor(id: number, scooterId: string, scooterHistoric: ScooterHistory[]) {
        this.id = id;
        this.scooterId = scooterId;
        this.scooterHistoric = scooterHistoric;
    }

    getLastHistory() {
        return this.scooterHistoric[this.scooterHistoric.length - 1];
    }
}

export class ScooterHistory {
    batteryLevel: number;
    longitude: number;
    latitude: number;
    createdAt: Date;

    constructor(batteryLevel: number, longitude: number, latitude: number, createdAt: Date) {
        this.batteryLevel = batteryLevel;
        this.longitude = longitude;
        this.latitude = latitude;
        this.createdAt = createdAt;
    }
}