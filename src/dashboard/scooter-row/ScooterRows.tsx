import React, {Component, useEffect, useState} from 'react';
import './ScooterRow.css';
import {Scooter, ScooterHistory} from './Scooter.model';
import {DataGrid, GridColDef, GridRowParams, GridValueSetterParams} from '@mui/x-data-grid';
import {AppProps} from '../../interfaces/AppProps';
// import {useNavigate} from "react-router-dom";

/*interface AppScootersState {
    scooters: Scooter[]
}*/

function ScooterRows (){

    // navigate = useNavigate();
    const [scooters, setScooters] = useState([]);
    let historyTmp: ScooterHistory[] = [];

    useEffect(() => {
        let mounted = true;

        getScooters()
            .then(async items => {
                const tmpScooters: Scooter[] = []
                for (const scooter of items) {
                    const history = await getLastHistory(scooter.id)
                    if (history) {
                        historyTmp = [new ScooterHistory(history.battery_level, history.longitude, history.latitude, history.created_at)]
                        let tmp = new Scooter(scooter.id, scooter.scoot_id,historyTmp)
                        tmpScooters.push(tmp);

                    }
                }
                setScooters(tmpScooters as any)
            })
        mounted = false;
    }, [])


    const columns: GridColDef[] = [
        {
            field: 'scooterId',
            headerName: 'ID du scooter',
            flex: 1,
            headerAlign: 'center',
            align: 'center',
        },
        {
            field: 'batteryLevel',
            headerName: 'Batterie',
            flex: 1,
            valueGetter: (params: GridValueSetterParams) => {
                return (params.row as Scooter).getLastHistory().batteryLevel.toString();
            },
            valueFormatter: params => `${params.value!} %`,
            headerAlign: 'center',
            align: 'center',
        },
        {
            field: 'longitude',
            headerName: 'Longitude',
            flex: 1,
            valueGetter: (params: GridValueSetterParams) => {
                return (params.row as Scooter).getLastHistory().longitude.toString();
            },
            headerAlign: 'center',
            align: 'center',
        },
        {
            field: 'latitude',
            headerName: 'Latitude',
            flex: 1,
            valueGetter: (params: GridValueSetterParams) => {
                return (params.row as Scooter).getLastHistory().latitude.toString();
            },
            headerAlign: 'center',
            align: 'center',
        },
    ];
    // eslint-disable-next-line react-hooks/rules-of-hooks


    return <div style={{height: 400, width: '100%', textAlign: "center"}}>
        <DataGrid
            rows={scooters} columns={columns}
            pageSize={5}
            rowsPerPageOptions={[5]}
            style={{textAlign: "center"}}
            onRowClick={params => goToScooterDetails(params)}
        />
    </div>;
}

function goToScooterDetails(params: GridRowParams) {
    window.location.href = `/${(params.row as Scooter).id}`;
}

function getScooters(){
    return fetch('https://apigateway.drfperso.ovh/api/scooters').then(scooter => scooter.json())
}

function getLastHistory(id: number){
    return fetch('https://apigateway.drfperso.ovh/api/scootHistory/'+id+'/last-history').then(lastHistory => lastHistory.json())
}

export default ScooterRows;