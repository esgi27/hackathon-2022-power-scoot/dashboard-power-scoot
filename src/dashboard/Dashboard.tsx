import React from 'react';
import './Dashboard.css';
import brumaire_logo from '../brumaire_logo.png';
import ScooterRows from './scooter-row/ScooterRows';
import {Button} from '@mui/material';

function Dashboard () {
    return (
        <div className="Dashboard">
            <header className="Dashboard-header">
                <h1>
                    Dashboard
                </h1>
                <img src={brumaire_logo} className="Dashboard-logo" alt="brumaire_logo" />
            </header>
            <body className="Dashboard-body">
                <Button className="Scooters-maps-button" variant="contained" onClick={() => window.location.href = "/scooters-fleet-maps"}>
                    Carte de la flotte
                </Button>
                <div className="Scooter-list">
                    <ScooterRows/>
                </div>
            </body>
        </div>
    );
}

export default Dashboard;
