import React, {Component} from 'react';
import {Scooter, ScooterHistory} from '../scooter-row/Scooter.model';
import { useState, useEffect } from "react"
import "./ScooterDetails.css";
import {AreaChart, CartesianGrid, XAxis, YAxis, Tooltip, Area} from 'recharts';
import brumaire_logo from '../../brumaire_logo.png';
import {useParams} from 'react-router-dom';

interface AppScooterState {
    scooters: Scooter
}

const ScooterDetails = (props: any) => {
    const [data, setData] = useState([]);
    const [refreshKey, setRefreshKey] = useState(0);

    const scooter = new Scooter(1, 'ID_3213', [  // TODO -> to change
        {
            latitude: 48.849127,
            longitude: 2.390424,
            batteryLevel: 91,
            createdAt: new Date()
        }
    ]);

    const params: any = useParams();

    useEffect(() => {
        const fetchScootHistoric = async () => {
            const res = await fetch(`https://apigateway.drfperso.ovh/api/scootHistory/${params.scooterId}`) // TODO -> to change
            const dataJson = await res.json();
            setData(dataJson.map(((scooterHistory: any) => {
                return {
                    battery_level: `${scooterHistory.battery_level}`,
                    created_at: new Date(scooterHistory.created_at).toLocaleDateString()
                }
            })));

            console.log('data')
            console.log(data)
        }
        fetchScootHistoric();
    }, [ data, params.scooterId, refreshKey ])

    return (
        <div className="Scooter-details">
            <header className="Scooter-details-header">
                <h1>
                    Scooter - {scooter.scooterId}
                </h1>
                <img src={brumaire_logo} className="Dashboard-logo" alt="brumaire_logo" />
            </header>
            <body className="Scooter-details-body">
                <div>
                    <h2>
                        Historique du niveau de batterie
                    </h2>
                    <AreaChart width={1000} height={500} data={data} margin={{top: 20, right: 5, left: 5, bottom: 50}}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <Area type="monotone" dataKey="battery_level" stroke="green" fill="lightgreen" activeDot={{ r: 8 }}/>
                        <Tooltip />
                        <XAxis dataKey="created_at"/>
                        <YAxis label={{ value: "Batterie", position: "insideLeft", angle: -90}}/>
                    </AreaChart>
                </div>
            </body>
        </div>
    );
}

export default ScooterDetails;