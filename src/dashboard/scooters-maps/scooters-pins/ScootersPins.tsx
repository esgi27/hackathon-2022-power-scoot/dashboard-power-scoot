
// @ts-ignore
import {Marker} from '@react-google-maps/api';
import React, {useEffect, useState} from 'react';
import {Container} from '@mui/material';
import {Scooter, ScooterHistory} from '../../scooter-row/Scooter.model';

function ScootersPins() {
    const [data, setData] = useState([]);
    const [refreshKey, setRefreshKey] = useState(0);


    useEffect(() => {
        const fetchScootersData = async () => {
            const scooters: Scooter[] = [];

            const res = await fetch(`https://apigateway.drfperso.ovh/api/scooters/`)
            const dataJson = await res.json();
            for (const scooterData of dataJson) {
                const historic = await fetch(`https://apigateway.drfperso.ovh/api/scootHistory/${scooterData.id}/last-history`)
                const historicData = await historic.json();
                const scooter = new Scooter(scooterData.id, scooterData.scoot_id, [
                    new ScooterHistory(historicData.battery_level, historicData.longitude, historicData.latitude, historicData.created_at)
                ]);
                scooters.push(scooter);
            }
            setData(scooters as any)

        }
        fetchScootersData();
    }, [ data, refreshKey ])

    // @ts-ignore
    return (
        <Container>
            {data.map(scooter => {
                console.log("debug before marker");
                console.log((scooter as Scooter).getLastHistory());
                return <Marker position={{lat: (scooter as Scooter).getLastHistory().latitude, lng: (scooter as Scooter).getLastHistory().longitude}}/>
            })}
        </Container>
    );

}

export default ScootersPins;