import React from 'react';
import './ScootersMaps.css';
import {GoogleMap, Marker, useJsApiLoader} from '@react-google-maps/api';
import brumaire_logo from '../../brumaire_logo.png';
import ScootersPins from './scooters-pins/ScootersPins';

const ScootersMaps = () => {
    const containerStyle = {
        width: '100%',
        height: '100%'
    };

    const center = {
        lat: 48.863402,
        lng: 2.384212,
    };

    const {isLoaded} = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyC1NozTMNEohSIPG63eCM4MBQs5klo5Rzc"
    })

    const [map, setMap] = React.useState(null)

    const onLoad = React.useCallback(function callback(map) {
        const bounds = new window.google.maps.LatLngBounds();
        map.fitBounds(bounds);
        setMap(map)
    }, [])

    const onUnmount = React.useCallback(function callback(map) {
        setMap(null)
    }, [])

    return isLoaded ? (
        <div className="Scooters-maps">
            <header className="Scooters-maps-header">
                <h1>
                    Localisation des scooters
                </h1>
                <img src={brumaire_logo} className="Dashboard-logo" alt="brumaire_logo" />
            </header>
            <body className="Scooters-maps-body">
                <div className="Scooters-fleet-maps">
                    <GoogleMap
                        mapContainerStyle={containerStyle}
                        center={center}
                        zoom={11}
                        // onLoad={onLoad}
                        // onUnmount={onUnmount}
                    >
                        <ScootersPins/>
                    </GoogleMap>
                </div>
            </body>
        </div>
    ): <div/>;
}


export default ScootersMaps;